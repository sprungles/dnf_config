# Faster Downloads/Parallel Download Support.

## You need Parallel Download support?

If you want faster download support, you need to do one of two things:

1) Add an Accelerator to Zypper, such as:

  a) Axel
     Installation instructions: "sudo zypper install axel"
    
     a) Make sure to go into /etc/zypp/zypp.conf and go to the bottom of the file. You will then add "downloader = /usr/bin/axel -a". You can then save the file and exit, or comment it with "## Adding support for downloader accelerator 'axel' above the file."

  b) aria2
     Installation instructions: "sudo zypper install aria2"

     b) Make sure to go into /etc/zypp/zypp.conf and go to the bottom of the file. You will then add "downloader = /usr/bin/aria2c". You can then save the file and exit, or comment it with "## Adding support for downloader accelerator 'aria2' above the file."

2) Install and use DNF in place of Zypper.
    
     This option is pretty extreme, but works. This will allow you to replace Zypper with DNF4, depending on what you are wanting to use. Here are the instructions to do so:

        Instructions to use DNF4: 
                
                You are more than welcome to use DNF4 I recommend you use Zypper, but if you REALLY want DNF4 for some reason, here is how you use it:

                    First, install DNF4: "sudo zypper install dnf". 

                    Next you will go ahead and edit your DNF configuration by doing one of two things. And those are:
                         1) Import my dnf configuration by first downloading using git.
                            
                            a) Install git by running "sudo zypper install git"

                            b1) run: "sudo zypper install dnf rpm-repos-openSUSE-Tumbleweed", this will add the opensuse repositories to /etc/yum.repos.d/ so dnf can use the openSUSE repos.

                            OR

                            b2) run: "sudo zypper install dnf libdnf-repo-config-zypp", this will add ALL of your current repositories to /etc/yum.repos.d/ so dnf can use ALL of your repos.

                            c1) run: "sudo zypper in PackageKit-backend-dnf"  to install the dnf backend.
                            c2) run: "sudo dnf swap PackageKit-backend-zypp PackageKit-backend-dnf" to switch out the backend to DNF.

                            d) run: "git clone https://gitlab.com/sprungles/dnf_config.git && cd dnf_config/ && sudo cp dnf.conf /etc/dnf/dnf.conf". This command will download and replace the dnf file for you.
                            e) run: "sudo dnf makecache" to refresh all of the repositories, and now DNF4 is ready to use!

                         2) run: "sudo nano /etc/dnf/dnf.conf" to edit the dnf configuration. You then will edit the file with the following text:
                         "# see `man dnf.conf` for defaults and possible options

                            [main]
                            gpgcheck=True
                            installonly_limit=3
                            clean_requirements_on_remove=True
                            best=False
                            skip_if_unavailable=True
                            parallel_downloads=40
                            fastestmirror=True
                            allow_vendor_change=yes"

                              If you have slower internet, drop the 40 to a lower number. that number is the max amount of parallel downloads that can happen. Now save the file and run "sudo dnf check-update && sudo dnf upgrade". You can now use dnf like normal.                   
                
                            a) Install git by running "sudo zypper install git"

                            b1) run: "sudo zypper install dnf rpm-repos-openSUSE-Tumbleweed", this will add the opensuse repositories to /etc/yum.repos.d/ so dnf can use the openSUSE repos.

                            OR

                            b2) run: "sudo zypper install dnf libdnf-repo-config-zypp", this will add ALL of your current repositories to /etc/yum.repos.d/ so dnf can use ALL of your repos.

                            c1) run: "sudo zypper in PackageKit-backend-dnf"  to install the dnf backend.
                            c2) run: "sudo dnf swap PackageKit-backend-zypp PackageKit-backend-dnf" to switch out the backend to DNF.

                            d) run: "git clone https://gitlab.com/sprungles/dnf_config.git && cd dnf_config.git && sudo cp dnf.conf /etc/dnf/dnf.conf". This command will download and replace the dnf file for you.
                            e) run: "sudo dnf makecache" to refresh all of the repositories, and now DNF4 is ready to use!

